" not compatible with vi
set nocompatible " vundle required
" turn on line numbers
set number

" hi screen line that the cursor is in
set cursorline

" filetype plugins
filetype plugin indent on

" syntax highlighting
syntax on

" autoread file change
set autoread

" copy to clipboard
set clipboard=unnamedplus
""""""
" UI

" highlight search results
set hlsearch

" show matching brackets under curser
set showmatch



""""""
" Tabs

" uses 2 spaces
set expandtab
set smarttab
set shiftwidth=2
set tabstop=2

" auto indent, smart indent
set ai
set si

""""""
" GVIM

" set font
set guifont=Ubuntu\ Mono\ 13
" no scrollbar, to toolbar
set guioptions=aegimLt

" gui tab name
" let t:tabname='foobar'
:set guitablabel=%{exists('t:tabname')?t:tabname\ :''}
""""""
" Aliases
" :mapclear
" :unmap
:command! W w
:command! T tabnew
:command! Cwd lcd %:p:h
" :map <C-S-v> "*p
:map <C-S-y> "*p
map <F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>
""""""""
" Ruby

""""""
" Vundle
so ~/.vim/plugins.vim 


colors webers
