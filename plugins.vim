"Vundle vim plugin installer
"install with :PluginInstall
set rtp+=~/.vim/bundle/Vundle.vim
filetype off " vundle required

call vundle#begin()

Plugin 'spf13/vim-colors'
Plugin 'flazz/vim-colorschemes'
Plugin 'sheerun/vim-polyglot'
" post vim-polyglot {{{
let ruby_operators = 1
let ruby_space_errors = 1
" these are variables from ruby.vim
let ruby_pseudo_operators = 1
let ruby_global_variable_error = 1
" allows operator highlighting
" trailing space, etc
let ruby_fold = 1
let ruby_foldable_groups = 'class def'
" do not want everything to fold

" My canges to ruby.vim
syn match rubyKeywordAsMethod "\%(\%(\.\@1<!\.\)\|&\.\|::\)\_s*\%([_[:lower:]][_[:alnum:]]*\|\%(BEGIN\|END\)\>\)"

" Bang and Predicate Methods and Operators {{{1
"! i seperated bool from bang !!!
syn match rubyBangPredicateMethod "\%(\h\|[^\x00-\x7F]\)\%(\w\|[^\x00-\x7F]\)*[!]"
syn match rubyBoolPredicateMethod "\%(\h\|[^\x00-\x7F]\)\%(\w\|[^\x00-\x7F]\)*[?]"


" }}}


Plugin 'w0rp/ale'

" colorized status line
Plugin 'itchyny/lightline.vim'
set laststatus=2 " lightline required

" rename current file with :Rename new_file_name.foo
Plugin 'danro/rename.vim'

" filetree; mapped to ctrl+o
Plugin 'scrooloose/nerdtree'
map <C-o> :NERDTreeToggle<CR>

" syntax checking
Plugin 'scrooloose/syntastic'
"   syntatstic's error message
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

"   auto update locations of errors
let g:syntastic_always_populate_loc_list = 0
let g:syntastic_auto_loc_list = 0
"   check syntax when opening buffer
let g:syntastic_check_on_open = 0
"   check syntax when saving
let g:syntastic_check_on_wq = 1

" putting rubocop first, bc sometimes gotta ignore reek
let g:syntastic_ruby_checkers = ['rubocop', 'mri', 'reek']
let g:syntastic_javascript_checkers = ['jshint']
let g:syntastic_scss_checkers = ['scss_lint']
let g:syntastic_coffee_checkers = ['coffee-jshint']
" haml checking is stupid
" let g:syntastic_haml_checkers = ['haml_lint']

call vundle#end()
filetype plugin indent on " vundle required

