" Vim color file
" Maintainer:   Weber
" Last Change:  2015/12/21
" URL:

" cool help screens
" :he group-name
" :he highlight-groups
"   - gives a list of groups w discription,  (e.g. Curson, LineNr)
" :he cterm-colors

hi clear
if exists("syntax_on")
    syntax reset
endif
let g:colors_name="webers"
"{{{
"2017: was basing these colors on solarized.
"2019:
"color cheat sheet: https://jonasjacek.github.io/colors/ 
"i wish i remembered how i picked these colors
"they arent from solarized
"had to change what is now base2 b/c it was the same as cyan
"names are based on solarized
"base are variants of grey base0-3 light to dark, 00-03 dark to light

let s:base0     = ' ctermfg=255 guifg=#eeeeee'
"Grey93
let s:base1     = ' ctermfg=251 guifg=#c6c6c6'
"Grey78
let s:base2     = ' ctermfg=147 guifg=#afafff'
"LightSteelBlue
let s:base3     = ' ctermfg=245 guifg=#8a8a8a'
"Grey54
let s:base03     = ' ctermfg=65 guifg=#5f875f'
"DarkSeaGreen4
let s:base02     = ' ctermfg=240 guifg=#585858'
"Grey35
let s:base01     = ' ctermfg=239 guifg=#4e4e4e'
"Grey30
let s:base00     = ' ctermfg=235 guifg=#262626'
"Grey15
let s:yellow    = ' ctermfg=136 guifg=#af8700'
"DarkGoldenrod
let s:orange    = ' ctermfg=166 guifg=#d75f00'
"DarkOrange3
let s:red       = ' ctermfg=124 guifg=#af0000'
"Red3
let s:magenta   = ' ctermfg=125 guifg=#af005f'
"DeepPink4
"TODO s:magenta.s:bgbase00 is hard to see
let s:violet    = ' ctermfg=61 guifg=#5f5faf'
"SlateBlue3
let s:blue      = ' ctermfg=33 guifg=#0087ff'
"DodgerBlue1
let s:cyan      = ' ctermfg=37 guifg=#00afaf'
"LightSeaGreen
let s:green     = ' ctermfg=64 guifg=#5f8700'
"Chartreuse4
let s:none      = ' ctermfg=NONE guifg=NONE'

let s:bgbase0     = " ctermbg=255 guibg=#eeeeee"
let s:bgbase1     = " ctermbg=251 guibg=#c6c6c6"
let s:bgbase2     = " ctermbg=147 guibg=#afafff"
let s:bgbase3     = " ctermbg=245 guibg=#8a8a8a"
let s:bgbase03    = " ctermbg=65 guibg=#5f875f"
let s:bgbase02    = " ctermbg=240 guibg=#585858"
let s:bgbase01    = " ctermbg=239 guibg=#4e4e4e"
let s:bgbase00    = " ctermbg=235 guibg=#262626"
let s:bgyellow    = " ctermbg=136 guibg=#af8700"
let s:bgorange    = " ctermbg=166 guibg=#d75f00"
let s:bgred       = " ctermbg=124 guibg=#af0000"
let s:bgmagenta   = " ctermbg=125 guibg=#af005f"
let s:bgviolet    = " ctermbg=61 guibg=#5f5faf"
let s:bgblue      = " ctermbg=33 guibg=#0087ff"
let s:bgcyan      = " ctermbg=37 guibg=#00afaf"
let s:bggreen     = " ctermbg=64 guibg=#5f8700"
let s:bgnone      = " ctermbg=NONE guibg=NONE"

" attributes are ignored for the 'Normal' Group
"see :help highlight-cterm for more (line 4580ish)
let s:bold      = ' term=bold cterm=bold gui=bold'
let s:underline = ' term=underline cterm=underline gui=underline'
let s:undercurl = ' term=undercurl cterm=undercurl gui=undercurl'
let s:reverse   = ' term=reverse cterm=reverse gui=reverse'
let s:ital      = ' term=italic cterm=italic gui=italic'
let s:standout  = ' term=standout cterm=standout gui=standout'
let s:ft_none   = ' term=NONE cterm=NONE gui=NONE'
""" }}}
" {{{
" i would like to reserve:
"  s:bgbase00 for main text stuff
"  s:yellow for main text stuff
"  s:yellow is for normal
"  s:base tag like things. like def. or ". things that open and close
"  s:cyan for var names
"  s:magenta for special variables
"  s:green 'identifiers'
"  s:orange for special 'identifiers'

let s:light = 1
if s:light == 1
  set background=light
  " {{{
  let s:fg0       = s:base00
  let s:fg1       = s:base01
  let s:fg2       = s:base02
  let s:fg3       = s:base03
  let s:bg0       = s:bgbase0
  let s:bg1       = s:bgbase1
  let s:bg2       = s:bgbase2
  let s:bg3       = s:bgbase3
  let s:comment   = s:ital.s:blue
  let s:normal    = s:fg0.s:bg0
  let s:tag0      = s:base00
  let s:tag1      = s:base01
  let s:tag2      = s:base02
  let s:tag3      = s:base03
  let s:var0      = s:cyan
  let s:var1      = s:magenta 
  let s:var2      = s:orange 
  let s:key0      = s:green
  " identifiers and the like
  let s:key1      = s:violet
  let s:key2      = s:blue
  let s:key3      = s:red
  let s:operator  = s:orange
  let s:operator2 = s:red
  "  }}}
else
  set background=dark
  " {{{
  let s:fg0       = s:base0
  let s:fg1       = s:base1
  let s:fg2       = s:base2
  let s:fg3       = s:base3
  let s:bg0       = s:bgbase00
  let s:bg1       = s:bgbase01
  let s:bg2       = s:bgbase02
  let s:bg3       = s:bgbase03
  let s:normal    = s:yellow.s:bg0
  let s:tag0      = s:base0
  let s:tag1      = s:base1
  let s:tag2      = s:base2
  let s:tag3      = s:base3
  let s:var0      = s:cyan
  let s:var1      = s:magenta 
  let s:var2      = s:magenta 
  let s:var3      = s:magenta 
  let s:key0       = s:green
  let s:key1       = s:green
  let s:key2       = s:green
  let s:key3       = s:green
  " identifiers and the like
  let s:operator  = s:orange
  let s:operator2 = s:red
  " }}}
endif
" }}}
""""""""""
" Default Groups
" TODO incomplete
" {{{

"hi ColorColumn
"hi Conceal
exe "hi! Cursor	"       .s:reverse.s:none.s:bgnone
exe "hi! CursorIM "     .s:reverse.s:fg0
"exe "hi CursorColumn "  .s:ft_none .s:bg3
exe "hi! CursorLine "   .s:underline.s:none.s:bgnone
" requires set cursorline
exe "hi! Directory "    .s:key0
"hi DiffAdd
exe "hi DiffChange"     .s:bold
"hi DiffDelete
"hi DiffText
"hi EndOfBuffer
exe "hi ErrorMsg"       .s:red .s:bg1
" commandline error message
exe "hi VertSplit"      .s:fg1.s:bg1
exe "hi Folded "        .s:none.s:fg0.s:bg1
exe "hi FoldColumn "    .s:ft_none .s:fg2 .s:bg3
exe "hi SignColumn"   .s:orange.s:bg1
exe "hi IncSearch "     .s:undercurl.s:bold
exe "hi LineNr	"       .s:fg2.s:bg1
" the line number"
exe 'hi CursorLineNr'   .s:fg0.s:bg1
exe "hi MatchParen"    .s:bold.s:bgnone
exe 'hi ModeMsg'       .s:ital.s:fg0.s:bg1
"e.g. --INSERT--
"hi MoreMsg
exe 'hi NonText'        .s:fg0.s:bg1
exe "hi Normal "        .s:normal
" default for all unmatched syntax
" Pmenu
" the Popup menu
" PmenuSel
" PmenuSbar
" PmenuThumb
exe "hi Question"     .s:green.s:bg1
" QuickFixLine
exe "hi Search "      .s:reverse
exe 'hi SpecialKey'   .s:underline.s:none.s:bgnone
" 'hi SpellBad
" 'hi SpellCap
" 'hi SpellLocal
" 'hi SpellRare
exe "hi StatusLine"   .s:fg0 .s:bg1
exe 'hi StatusLineNC' .s:fg0.s:bg1
" StatusLine of noncurrent windows
exe 'hi StatusLineTerm' .s:fg1.s:bg2
" if terminal
exe 'hi StatusLineNCTerm' .s:fg1.s:bg2
exe 'hi TabLine'      .s:fg3.s:bg2
exe 'hi TabLineFill'  .s:fg3.s:bg2
exe 'hi TabLineSelect'  .s:fg3.s:bg2
exe 'hi Terminal'     .s:yellow.s:bg0
exe "hi Title"        .s:fg2 .s:bgnone
exe "hi Visual "      .s:none .s:bg1
exe "hi VisualNOS"    .s:none.s:bgred
" not owning selection
exe "hi WarningMsg"   .s:orange.s:bg2
" warning messages
exe "hi WildMenu"     .s:orange.s:bgmagenta
" }}}
""""""""
" GUI
" incomplete
" {{{

"hi Menu
"hi Scrollbar
"hi Tooltip

" }}}
""""""
" Syntax Groups
" syntax groups inherit from the preferred * group
" {{{

" *Comment
exe "hi Comment "         s:comment

" *Constant
exe "hi Constant	"       .s:bold .s:var0 .s:bgnone
exe "hi String	"         .s:ital .s:var0 .s:bgnone
exe "hi Character	"       .s:ital .s:var0 .s:bgnone
exe "hi Number	"         .s:ital .s:var0 .s:bgnone
exe "hi Boolean	"         .s:ital .s:var0 .s:bgnone
exe "hi Float	"           .s:ital .s:var0 .s:bgnone

" *Identifier
exe "hi Identifier	"     .s:ft_none .s:var1 .s:bgnone
" any variable name
exe "hi Function"         .s:key0.s:bgnone
" function/method name

" *Statement
exe "hi Statement "       .s:key1 .s:bgnone
"e.g. super, the do in blah.each do
exe "hi Conditional "     .s:tag1 .s:bgnone
" e.g. if, else
exe 'hi Repeat'           .s:tag2.s:bgnone
" e.g. for, do, while
exe "hi Label "           .s:tag3 .s:bgnone
" e.g. case, default
exe 'hi Operator'         .s:operator
exe 'hi Keyword'          .s:ft_none.s:key0
" any other keyword
exe "hi Exception "       .s:key1.s:bgnone

" *PreProc
exe "hi PreProc "         .s:key0 .s:bgnone
" exe "hi Include "         .s:fg2 .s:bgnone
hi link Define            Type
exe "hi Macro"            .s:var1.s:bgnone
" nerdtree ln
exe "hi PreCondit"        .s:tag2.s:bgred

" *Type
exe "hi Type	"           .s:tag1 .s:bgnone
" class name, etc
" the only one that doesnt get overwritten by ruby
exe "hi StorageClass"     .s:key1.s:bgnone
" like var in javascript
exe "hi Structure"        .s:key0.s:bgmagenta
exe "hi Typedef"          .s:key0.s:bgmagenta

" *Special
exe "hi Special "	        .s:key1.s:bgnone
" e.g. the '.' in blah.each
exe "hi SpecialChar "     .s:key1 .s:bgblue
exe "hi Tag "             .s:underline .s:blue .s:fg0
"you can use ctrl-] on this
exe "hi Delimiter "       .s:tag2 .s:bgnone
exe "hi SpecialComment"   .s:key1.s:bgred
" special things inside a comment
exe "hi Debug"            .s:key1.s:bggreen

" *Underlined
exe "hi Underlined	"     .s:underline.s:none.s:bgnone

" *Ignore
exe "hi Ignore"         .s:fg1.s:bgnone.s:bgmagenta
" not working?

" *Error
exe "hi Error "           .s:red.s:bg1
" used in the left SignColumn to point out errors
exe "hi SyntasticErrorSign " .s:red.s:bg1
exe "hi SyntasticWarningSign " .s:blue.s:bg1

" *ToDo
exe "hi Todo	"           .s:ft_none .s:magenta .s:bg1
" keywords TODO FIXME and XXX
" }}}

"" My Groups
" {{{
" hi Block
" hi ASCIICode
" hi Access
" hi AliasDeclariation
" hi Array

exe "hi Access "                  .s:key1 .s:bgnone
"e.g. private
hi link AliasDeclaration Declaration
exe "hi Anchor "            .s:key1 .s:bg0
exe "hi AngleBrackets "     .s:red .s:bgmagenta
hi link Argument                  ParameterList
hi link ArrayDelimiter Delimiter
"e.g. [ this, and-this ]
hi link ArrayLiteral Literal
exe "hi ASCIICode "               .s:var1.s:bgmagenta
hi link AssignmentOperator Operator
exe "hi AssignmentOperator "       .s:operator2
exe "hi Attribute "               .s:key0
hi link BangPredicateMethod PredicateMethod
exe "hi BangPredicateMethod"      .s:operator2
exe "hi BeginEnd "                .s:tag2
exe "hi Block "              .s:ft_none
" e.g. the < in: class Class < Bigclass
hi link BlockArgument Argument
hi link BlockDelimiter Delimiter
hi link BlockExpression Expression
" "e.g. begin \n this \n end
hi link BlockParameter Parameter
" | |
hi link BlockParameterList ParameterList
" |this|
hi link BoolPredicateMethod PredicateMethod
hi link Bracket         ParameterList
hi link Brackets Bracket
hi link BracketOperator Brackets
hi link CaseExpression Expression
exe "hi CharClass "         .s:var0      .s:bg0
exe "hi Class"                    .s:tag0.s:bgnone
" the word 'class'
hi link ClassDeclaration Declaration
hi link ClassVariable Variable
hi link ConditionalExpression Expression
" "e.g. if this \n and-this \n end
"TODO change modifier
hi link ConditionalModifier Modifier
exe "hi Control "                 .s:tag2 .s:bgnone
" "e.g. do, next, return, begin, end
hi link CurlyBlock Block
hi link CurlyBlockDelimiter BlockDelimiter
" "e.g. { }
exe "hi CurlyBraces "       .s:red .s:bgmagenta
exe "hi Data "                    .s:red .s:bgmagenta
hi link DataDirective Directive
exe "hi Declaration "        .s:key0 .s:bgviolet
hi link DelimEscape Escape
exe "hi Directive "           .s:red .s:bgmagenta
exe "hi Do "              .s:red .s:bgmagenta
hi link Dot               Type
exe "hi DoLine "          .s:ital.s:var1
hi link DoBlock Block
exe "hi DoBlock"          .s:ital
exe "hi Documentation "           .s:red .s:bgmagenta
exe "hi Escape "             .s:key0 .s:bgnone
exe "hi Eval "                    .s:key1
exe "hi Exceptional "             .s:key1.s:bgnone
" "e.g. rescue
exe "hi Expression "        .s:ital.s:yellow.s:bgnone
hi link GlobalVariable Variable
" "e.g. $LAST_READ_LINE
exe "hi Heredoc "                 .s:red .s:bgmagenta
hi link HeredocStart Start
" "e.g. def this
hi link InstanceVariable Variable
hi link Integer Number
exe "hi Interpolation "         .s:red .s:bgmagenta
exe "hi Intepolation "            .s:red .s:bgmagenta
hi link IntepolationDelimiter Delimiter
hi link InvalidVariable Variable
hi link KeywordAsMethod Method
exe "hi Literal "            .s:var0 .s:bgnone
hi link LocalVariableOrMethod Variable
exe "hi Method"       .s:ft_none.s:key1.s:bgnone
hi link MethodBlock Block
hi link MethodDeclaration Declaration
" "e.g. def self.foo ( the '.' )
hi link MethodException Exception
exe "hi Modifier "          .s:key0
hi link Modifier Condition
exe "hi Module "                  .s:red.s:bgmagenta
hi link ModuleDeclaration Declaration
hi link MultilineComment Comment
hi link NestedAngleBrackets AngleBrackets
hi link NestedCurlyBraces CurlyBraces
hi link NestedParentheses Parens
hi link NestedSquareBracket SquareBracket
hi link NoInterpolation Interpolation
exe "hi Operator "           .s:operator
hi link OptionalDo Do
hi link OptionalDoLine DoLine
" " e.g. while this
exe "hi Parameter "          .s:key0 .s:bgnone
hi link Parens               ParameterList
exe "hi ParameterList "      .s:var1 .s:bgnone
hi link PredicateMethod Method
" "e.g. Nokogiri::HTML() (the word HTML)
" "e.g. def foo \n this \n and-this \n end
hi link PredefinedConstant Constant
" " e.g. ENV[]
hi link PredefinedIdentifier Identifier
exe "hi PredefinedIdentifier "    .s:var0 .s:bgblue
hi link PredefinedVariable Variable
exe "hi PredefinedVariable "      .s:var0 .s:bgnone
" e.g. $_
hi link ProperOperator Operator
hi link PseudoVariable Variable
" e.g. self
exe "hi Quantifier "        .s:operator
hi link QuoteEscape Escape
" "e.g. '\''
" not working??
exe "hi Regexp "                  .s:key0     .s:bg0
hi link RegexpAnchor Anchor
hi link RegexpBrackets Brackets
hi link RegexpCharClass CharClass
hi link RegexpComment Comment
exe "hi RegexpComment "           .s:comment  .s:bg0
hi link RegexpDelimiter Delimiter
exe "hi RegexpDelimiter "         .s:bg0
hi link RegexpDot Dot
exe "hi RegexpDot "               .s:bg0
hi link RegexpEscape Escape
exe "hi RegexpEscape "            .s:bg0
hi link RegexpParens Parens
exe "hi RegexpParens "            .s:bg0
hi link RegexpQuantifier Quantifier
exe "hi RegexpQuantifier "        .s:bg0
hi link RegexpSpecial Special
exe "hi RegexpSpecial "           .s:bg0
" "e.g. |
hi link RepeatExpression Expression
" "e.g. while blah \n this \n end
hi link RepeatModifier Modifier
hi link rubyBoolPredicateMethod PredicateMethod
hi link rubyBangPredicateMethod PredicateMethod
exe "hi SharpBang "               .s:operator2
hi link SpaceError Error
exe "hi Special "           .s:key1
exe "hi SquareBracket "     .s:red .s:bgmagenta
exe "hi Start "            .s:red .s:bgmagenta
hi link StringDelimiter Delimiter
hi link StringEscape Escape
" "e.g. \n etc
exe "hi Symbol "                  .s:var0
hi link SymbolDelimiter Delimiter
exe "hi Variable "      .s:var0 .s:bgnone

" }}}


"""""""""""
" Language Specific
" sorted by alphabet
" ANT does javascript and python
"
"" JavaScript
" {{{
hi link jsBracket                 Bracket
hi link jsFuncArgs                Argument 
hi link jsFunctionKey             Method
hi link jsFuncBlock               Block 
hi link jsFuncCall                Method
hi link jsObjectKey               Method
hi link jsObjectProp              Variable
hi link jsObjectValue           val0
hi link jsParen                Parens
hi link jsVariableDef             GlobalVariable
hi link Noise                     Dot
" }}}

""""" Haml
" {{{
exe "hi hamlRuby " .s:yellow
hi link hamlAttributesHash  Symbol
" }}}
"""""""""""
" Ruby Stuff
" {{{
" unknowns get bgmagenta
" linking to a main group (e.g. rubyOperator to Operator)
" doesnt seem necessary but doing it anyways
" to be thorough, & besides, may want to change it someday

"c. vim-polyglot/syntax/ruby.vim
hi link erubyBlock rubyBlock
exe "hi erubyOneLiner "                .s:yellow .s:bgred


hi link rubyASCIICode ASCIICode
hi link rubyAccess Access
"e.g. private
hi link rubyAliasDeclaration AliasDeclaration
exe "hi rubyAliasDeclaration2 "       .s:key1 .s:bgmagenta
hi link rubyArrayDelimiter ArrayDelimiter
hi link rubyArrayLiteral ArrayLiteral
"e.g. [ this, and-this ]
hi link rubyAttribute Attribute
hi link rubyBangPredicateMethod BangPredicateMethod
hi link rubyBoolPredicateMethod BoolPredicateMethod
hi link rubyBeginEnd BeginEnd
hi link rubyBlock Block
" e.g. the < in: class Class < Bigclass
hi link rubyBlockArgument BlockArgument
hi link rubyBlockExpression BlockExpression
" "e.g. begin \n this \n end
hi link rubyBlockParameter BlockParameter
" | |
hi link rubyBlockParameterList BlockParameterList
" |this|
hi link rubyBoolean Boolean
hi link rubyBracketOperator BracketOperator
hi link rubyCaseExpression CaseExpression
hi link rubyClass Class
" the word 'class'
hi link rubyClassDeclaration ClassDeclaration
hi link rubyClassVariable ClassVariable
hi link rubyComment Comment
hi link rubyConditional Conditional
hi link rubyConditionalExpression ConditionalExpression
" "e.g. if this \n and-this \n end
hi link rubyConditionalModifier ConditionalModifier
hi link rubyConstant Constant
hi link rubyControl Control
" "e.g. do, next, return, begin, end
hi link rubyCurlyBlock CurlyBlock
hi link rubyCurlyBlockDelimiter CurlyBlockDelimiter
" "e.g. { }
hi link rubyData Data
hi link rubyDataDirective DataDirective
hi link rubyDefine Define
hi link rubyDelimEscape DelimEscape
hi link rubyDoBlock DoBlock
hi link rubyDocumentation Documentation
hi link rubyError Error
hi link rubyEscape Escape
hi link rubyEval Eval
hi link rubyException Exception
hi link rubyExceptional Exceptional
" "e.g. rescue
hi link rubyFloat Float
hi link rubyFunction Function
hi link rubyGlobalVariable GlobalVariable
" "e.g. $LAST_READ_LINE
hi link rubyHeredoc Heredoc
hi link rubyHeredocStart HeredocStart
hi link rubyIdentifier Identifier
" "e.g. def this
hi link rubyInclude Include
hi link rubyInstanceVariable InstanceVariable
hi link rubyInteger Integer
hi link rubyIntepolation Interpolation
" should this be inteRpolation?
hi link rubyIntepolationDelimiter interpolationDelimiter
hi link rubyInvalidVariable InvalidVariable
hi link rubyKeyword Keyword
hi link rubyLocalVariableOrMethod LocalVariableOrMethod
" "e.g. Nokogiri::HTML() (the word HTML)
hi link rubyMethodBlock MethodBlock
hi link rubyKeywordAsMethod KeywordAsMethod
" doesnt work, dunno why
" "e.g. def foo \n this \n and-this \n end
hi link rubyMethodDeclaration MethodDeclaration
" "e.g. def self.foo ( the '.' )
hi link rubyMethodException MethodException
hi link rubyModule Module
hi link rubyModuleDeclaration ModuleDeclaration
hi link rubyMultilineComment MultilineComment
hi link rubyNestedAngleBrackets NestedAngleBrackets
hi link rubyNestedCurlyBraces NestedCurlyBraces
hi link rubyNestedParentheses NestedParentheses
hi link rubyNestedSquareBracket NestedSquareBracket
hi link rubyNoInterpolation NoInterpolation
hi link rubyOperator Operator
hi link rubyOperator Operator
hi link rubyProperOperator ProperOperator
hi link rubyAssignmentOperator AssignmentOperator
hi link rubyOptionalDo OptionalDo
hi link rubyOptionalDoLine OptionalDoLine
" " e.g. while this
hi link rubyParentheses Parens
hi link rubyPredefinedConstant PredefinedConstant
" " e.g. ENV[]
hi link rubyPredefinedIdentifier PredefinedIndetifier
hi link rubyPredefinedVariable PredefinedVariable
" e.g. $_
hi link rubyPseudoVariable PseudoVariable
" e.g. self
hi link rubyQuoteEscape rubyQuoteEscape
" "e.g. '\''
" not working??
hi link rubyRegexp Regexp
hi link rubyRegexpAnchor RegexpAnchor
hi link rubyRegexpBrackets RegexpBrackets
hi link rubyRegexpCharClass RegexpCharClass
hi link rubyRegexpComment RegexpComment
hi link rubyRegexpDelimiter RegexpDelimiter
hi link rubyRegexpDot RegexpDot
hi link rubyRegexpEscape RegexpEscape
hi link rubyRegexpParens RegexpParens
hi link rubyRegexpQuantifier RegexpQuantifier
hi link rubyRegexpSpecial RegexpSpecial
" "e.g. |
hi link rubyRepeat Repeat
hi link rubyRepeatExpression RepeatExpression
" "e.g. while blah \n this \n end
hi link rubyRepeatModifier RepeatModifier
hi link rubySharpBang SharpBang
hi link rubySpaceError SpaceError
hi link rubyString String
hi link rubyStringDelimiter Delimiter
hi link rubyStringEscape StringEscape
" "e.g. \n etc
hi link rubySymbol Symbol
hi link rubySymbolDelimiter SymbolDelimiter
hi link rubyTodo Todo
" 
" "exe "hi htmlTag "                      .s:yellow .s:bgred


" }}}
" vim:foldmethod=marker

"" Vim
" {{{
hi link vimGroupName Identifier

" }}}
